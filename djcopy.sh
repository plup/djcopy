#!/bin/bash
set -e

load_config() {
    if [[ ! -e "$1" ]]; then
        echo "Config file '$1' not found"
        exit 1
    fi
    . "$1"
}

copy_files() {

    echo -n 'Copying media files... '
    if [[ ! -z ${origin_media} ]]; then
       nice -n 10 rsync --compress --delete-after --archive --partial \
           "${origin_user:?}@${origin_host:?}:${origin_media:?}/" \
           "${local_media:?}"
       echo "OK"
    else
       echo "Skipped"
    fi

    echo -n 'Copying static files... '
    if [[ ! -z ${origin_static} ]]; then
        nice -n 10 rsync --compress --delete-after --archive --partial \
            "${origin_user:?}@${origin_host:?}:${origin_static:?}/" \
            "${local_static:?}"
        echo "OK"
    else
        echo "Skipped"
    fi
    
    echo -n 'Copying django code... '
    if [[ ! -z ${origin_django} ]]; then
        nice -n 10 rsync --compress --delete-after --archive --partial \
            "${origin_user:?}@${origin_host:?}:${origin_django:?}/" \
            "${local_django:?}"
        echo "OK"
    else
        echo "Skipped"
    fi

}

copy_db() {
    echo -n 'Copying database... '

    dump_file=dump_dj_$(date +%F)

    # Dump on oririn through SSH
    #ssh ${origin_user:?}@${origin_host:?} "PGPASSWORD=${origin_db_pass} pg_dump -Fc -f \$HOME/$dump_file -h ${origin_db_host} -U ${origin_db_user} -d ${origin_db_name}"
    ssh ${origin_user:?}@${origin_host:?} "PGPASSWORD=${origin_db_pass} pg_dump -Fc --clean --if-exists -n public -f \$HOME/$dump_file -h ${origin_db_host} -U ${origin_db_user} -d ${origin_db_name}"

    # Copy dump to destination
    nice -n 10 rsync --compress --delete-after --archive --partial \
        ${origin_user:?}@${origin_host:?}:\$HOME/$dump_file \
        $HOME/$dump_file

    # Remove dump on origin
    ssh ${origin_user:?}@${origin_host:?} "rm \$HOME/$dump_file"

    # Empty destination db
    PGPASSWORD=${local_db_pass} psql -h ${local_db_host} -U ${local_db_user} -d ${local_db_name} \
        -t -c "select 'drop table if exists \"' || tablename || '\" cascade;' from pg_tables where schemaname = 'public';" | PGPASSWORD=${local_db_pass} PGOPTIONS='--client-min-messages=warning' psql --quiet -h ${local_db_host} -U ${local_db_user} -d ${local_db_name}

    # Import dump on destination
    PGPASSWORD=${local_db_pass} pg_restore -Fc --clean --if-exists -n public --no-acl --no-owner --role=${local_db_user} -h ${local_db_host} -U ${local_db_user} -d ${local_db_name} $HOME/$dump_file || true

    # Remove local dump
    rm $HOME/$dump_file

    echo "OK"
}

# parsing args
usage() {
    echo "Usage: $0 [-c config_file] [-d] [-f]"
}

while getopts "hc:df" opt; do
    case ${opt} in
        h )
            usage
            exit 0
        ;;
        c )
            load_config ${OPTARG}
        ;;
        d )
            copy_db
        ;;
        f )
            copy_files
        ;;
        \? )
          echo "Invalid Option: -$OPTARG" 1>&2
          exit 1
        ;;
  esac
done
shift $((OPTIND -1))
