# DJ COPY

## Requirements

This script relies on:

* `SSH` authentication from target server to origin
* `postgres` backend on both, target and origin, servers
* `virtualenv` or python environnment setup for the django

## Install

Drop script on target server:
```
$ curl -O https://gitlab.com/plup/djcopy/raw/master/djcopy.sh
```

## Configuration

### Parameters

Configure parameters in a `djcopy.conf` file:
```
# Origin host
origin_host=example.org
origin_user=sshuser

# Directories on origin (leave empty to skip)
origin_django= 
origin_static=
origin_media=\$HOME/media
    
# Local directories
local_django=
local_static=
local_media=~/media

# Databases config on origin
origin_db_host=localhost
origin_db_user=dbuser
origin_db_name=dbname
origin_db_pass=dbpass

# Local database config
local_db_host=localhost
local_db_user=dbuser
local_db_name=dbname
local_db_pass=dbpass
```

### Launch script

Trigger the copy:
```
$ bash djcopy.sh -c djcopy.conf -df
```

Have a beer !
```
   _.._..,_,_
  (          )
   ]~,"-.-~~[
 .=])' (;  ([
 | ]:: '    [
 '=]): .)  ([
   |:: '    |
    ~~----~~
```
